<?php

use yii\db\Migration;

/**
 * Handles the creation of table `guest_messages`.
 */
class m180917_032308_create_guest_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('guest_messages', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(15)->notNull(),
            'browser' => $this->string(30)->notNull(),
            'user_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'home_page' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->timestamp()->notNull()
        ], $tableOptions);

        $this->createIndex('idx-guest_messages-user_name','guest_messages', 'user_name');
        $this->createIndex('idx-guest_messages-email','guest_messages', 'email');
        $this->createIndex('idx-guest_messages-created_at','guest_messages', 'created_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('guest_messages');
    }
}
