<?php

namespace app\models;

use Carbon\Carbon;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 *
 * @property string $user_name
 * @property string $email
 * @property string $home_page
 * @property string $text
 * @property string $verifyCode
 */
class MessageForm extends Model
{
    public $user_name;
    public $email;
    public $home_page;
    public $text;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            /** 'user_name', 'email', 'text' are required */
            [['user_name', 'email', 'text'], 'required'],

            ['home_page', 'url'],
            ['email', 'email'],

            [['user_name'], 'safe'],

            /** verifyCode needs to be entered correctly*/
            ['verifyCode', 'captcha', 'captchaAction'=> 'book/captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Captcha',
            'user_name' => 'ФИО',
            'email' => 'Email',
            'homepage' => 'Добашняя страница',
            'text' => 'Текст',
        ];
    }

    public function add()
    {
        $_SERVER['HTTP_USER_AGENT'];

        if ($this->validate()) {
            $message = new GuestMessages();
            $message->user_name = $this->user_name;
            $message->email = $this->email;
            $message->home_page = $this->home_page;
            $message->text = $this->text;

            $message->browser = $this->getBrowser();
            $message->ip = Yii::$app->request->userIP;

            $message->created_at = Carbon::now();

            $message->save();

            return true;
        }
        return false;
    }

    function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';

        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
        }

        return $bname;
    }

}
