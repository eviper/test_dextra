<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GuestMessages;

/**
 * GuestMessagesSearch represents the model behind the search form of `app\models\GuestMessages`.
 */
class GuestMessagesSearch extends GuestMessages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ip', 'browser', 'user_name', 'email', 'home_page', 'text', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuestMessages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
            'sort' => ['attributes' => ['user_name','email', 'created_at']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'created_at' => $this->created_at,
//        ]);
//
//        $query->andFilterWhere(['like', 'ip', $this->ip])
//            ->andFilterWhere(['like', 'browser', $this->browser])
//            ->andFilterWhere(['like', 'user_name', $this->user_name])
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'home_page', $this->home_page])
//            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
