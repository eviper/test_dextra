<?php

namespace app\models;

use Carbon\Carbon;
use Faker\Factory;
use Yii;

/**
 * This is the model class for table "guest_messages".
 *
 * @property int $id
 * @property string $ip
 * @property string $browser
 * @property string $user_name
 * @property string $email
 * @property string $home_page
 * @property string $text
 * @property string $created_at
 */
class GuestMessages extends \yii\db\ActiveRecord
{

    const GEN_SIZE = 75;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guest_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'browser' => 'Браузер',
            'user_name' => 'ФИО',
            'email' => 'Email',
            'home_page' => 'Страница',
            'text' => 'Текст',
            'created_at' => 'Создано',
        ];
    }


    /**
     * Generate guest messages
     */
    public static function generate()
    {
        $factory = Factory::create();
        $data = [];
        $browsers = ['Unknown', 'Internet Explorer', 'Mozilla Firefox', 'Google Chrome', 'Apple Safari', 'Opera', 'Netscape'];
        $browsersSize = count($browsers);

        for ($i = 0; $i < self::GEN_SIZE; $i++){
            $data[] = [
                $factory->ipv4,
                $browsers[$i % $browsersSize],
                $factory->name,
                $factory->email,
                $factory->url,
                $factory->text,
                Carbon::now()
            ];
        }
        $connection = Yii::$app->db;
        try{
            if($data)
                $connection->createCommand()->batchInsert(self::tableName(),
                    ['ip', 'browser', 'user_name', 'email', 'home_page', 'text', 'created_at'],
                    $data
                )->execute();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }

    }

    /**
     * Remove all guest messages
     */
    public static function clear()
    {
        self::deleteAll();
    }
}
