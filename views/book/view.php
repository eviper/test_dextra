<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GuestMessages */

$this->title = $model->user_name . '#' . $model->id;
\yii\web\YiiAsset::register($this);
?>
<div class="guest-messages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить сообщение?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ip',
            'browser',
            'user_name',
            'email:email',
            'home_page',
            'text:ntext',
            'created_at',
        ],
    ]) ?>

</div>
