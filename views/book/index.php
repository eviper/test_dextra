<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GuestMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообшения пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-messages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(
            'добавить ' . \app\models\GuestMessages::GEN_SIZE,
            ['/gen'],
            [
                'class' => 'btn btn-lg btn-primary',
                'data' => [
                    'method' => 'post',
                ],
            ]
        ) ?>
        <?= Html::a(
            'Удалить все',
            ['/clear'],
            [
                'class' => 'btn btn-lg btn-warning',
                'data' => [
                    'confirm' => 'Вы уверены что хотите удалить сообщения?',
                    'method' => 'post',
                ],
            ]
        ) ?>
    </p>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'value' => function (\app\models\GuestMessages $data) {
                    return Html::a(Html::encode($data->id), \yii\helpers\Url::to(['view', 'id' => $data->id]));
                },
                'format' => 'raw',
            ],

            'user_name',

            'email:email',
            'home_page',
            'ip',
            'browser',

//            'text:ntext',
            'created_at',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
