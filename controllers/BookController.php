<?php

namespace app\controllers;

use app\models\MessageForm;
use Yii;
use app\models\GuestMessages;
use app\models\GuestMessagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookController implements the CRUD actions for GuestMessages model.
 */
class BookController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all GuestMessages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuestMessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GuestMessages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return Response|string
     */
    public function actionCreate()
    {
        $message = new MessageForm();
        if ($message->load(Yii::$app->request->post()) && $message->add()) {
            Yii::$app->session->setFlash('success', 'Сообщение успешно добавлено');

            return $this->redirect(['index']);
        }
        return $this->render('message', [
            'model' => $message,
        ]);
    }


    /**
     * Generate new guest messages
     * @return mixed
     */
    public function actionGen()
    {
        GuestMessages::generate();
        return $this->redirect(['index']);
    }


    /**
     * Remove all guest messages
     * @return mixed
     */
    public function actionClear()
    {
        GuestMessages::clear();
        return $this->redirect(['index']);
    }


    /**
     * Deletes an existing GuestMessages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GuestMessages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuestMessages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GuestMessages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
